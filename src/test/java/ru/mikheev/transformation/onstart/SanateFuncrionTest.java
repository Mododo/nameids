package ru.mikheev.transformation.onstart;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static ru.mikheev.transformation.onstart.SanateFunction.*;

public class SanateFuncrionTest {

    @DataProvider
    public Object[][] dates(){
        return new String[][]{
                {"1990.1.1","1990.01.01"},
                {"1992.02.2", "1992.02.02"},
                {"1992.2.2", "1992.02.02"},
                {"92.2.27", "1992.02.27"},
                {"2.12.92", "1992.12.02"},
        };
    }

    @Test(dataProvider = "dates")
    public void sanateBdateTest(String[] bdates){
        Assert.assertEquals(sanateBdate(bdates[0]), bdates[1]);
    }

    @DataProvider
    public Object[][] stringWithDuplicates(){
        return new String[][]{
                {"111111aada111aa","1ada1a"},
                {"Ббборис", "Бборис"}
        };
    }

    @Test(dataProvider = "stringWithDuplicates")
    public void sequenceOfUniquesTest(String[] s){
        Assert.assertEquals(sequenceOfUniques(s[0]).toString(), s[1]);
    }

    @DataProvider
    public Object[][] simpleNames(){
        return new String[][]{
                {"XENYA","XENIA"},
                {"KSENIJA", "XENIIA"},
                {"YANA", "IANA"},
                {"IVANOVA ", "IVANOV "}
        };
    }

    @Test(dataProvider = "simpleNames")
    public void applyReplacementsTest(String[] s){
        Assert.assertEquals(applyReplacements(new StringBuilder(s[0])).toString(), s[1]);
    }

    @DataProvider
    public Object[][] names(){
        return new String[][]{
                {"Bor. borissovitch; zeljentsov","BOR~ BORISOVICH ZELENTSOV"},
                {"Yana A. Petrova","IANA A~ PETROV"},
                {"Jana Alex. Petrovva","IANA ALEX~ PETROV"},
                {"Ksenija, Ivanoff","XENI IVANOV"},
                {"Ivanova, Xeniya Pavlovna","IVANOV XENI PAVLOVNA"},
                {"Maxim Andreevich Sidorov","MAXIM ANDREVICH SIDOROV"},
                {"Kirill Sergeevich Mikheev","KIRIL SERGEVICH MIHEV"},
        };
    }

    @Test(dataProvider = "names",
            dependsOnMethods = {"sequenceOfUniquesTest", "applyReplacementsTest"})
    public void sanateFioTest(String[] s){
        Assert.assertEquals(sanateFio(s[0]), s[1]);
    }

}
