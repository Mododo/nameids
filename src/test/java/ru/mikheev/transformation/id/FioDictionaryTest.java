package ru.mikheev.transformation.id;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static ru.mikheev.transformation.id.FioDictionary.compareNames;
import static ru.mikheev.util.Permutations.permute;

public class FioDictionaryTest {
    /**
     * Проверяет рефлексивность f(a,a) == 0
     */
    @Test(dataProvider = "singles")
    public void reflexivity(String[] name) {
        System.out.println("reflexivity for: " + Arrays.toString(name));
        Assert.assertEquals(compareNames(name, name), 0);
    }

    /**
     * Проверяет симметричность f(a,b) == f(b,a)
     */
    @Test(dataProvider = "tuples")
    public void symmetry(String[][] arr) {
        int a = compareNames(arr[0], arr[1]);
        System.out.println("symmetry for: " + Arrays.toString(arr[0])
                + " " + a + " " + Arrays.toString(arr[1]));
        Assert.assertEquals(compareNames(arr[1], arr[0]), -a);
    }

    /**
     * Проверяет транзитивность f(a,b) == f(b,c) == f(a,с)
     * и отношение полного порядка f(a,b) > f(b,c) => a>=b>c или a>b>=c => f(a,c) > 0
     * который нарушается, когда в сравнение попадают фио из двух частей и фио с сокращением
     */
    @Test(dataProvider = "triples")
    public void transitivity(String[][] arr) {
        int _01 = compareNames(arr[0], arr[1]);
        int _12 = compareNames(arr[1], arr[2]);
        int _02 = compareNames(arr[0], arr[2]);
        System.out.println("transitivity: " +
                Arrays.toString(arr[0]) + " " + _01 + " " +
                Arrays.toString(arr[1]) + " " + _12 + " " +
                Arrays.toString(arr[2]) + " => " + _02);
        Assert.assertEquals(_02, Integer.signum(_01 + _12));
    }

    @DataProvider
    public Object[][] singles() {
        return new String[][]{
                new String[]{"Alex", "Alexeev", "A~"},
                new String[]{"A","Alexeevich","Alex~"},
                new String[]{"Alexeev","Alexey"},
                new String[]{"Alexey", "Borisov", "R~"},
                new String[]{"Borisov", "Ruslan", "B~"},
                new String[]{"Alexey", "Borisov", "B~"},
                new String[]{"Boris", "Borisov", "Boris~"},
                new String[]{"Boris", "Borisov", "Bogdan~"},
                new String[]{"Boris", "Borisov"}
        };
    }

    @DataProvider
    public Object[][] tuples() {
        return permute((String[][]) singles(), 2);
    }

    @DataProvider
    public Object[] triples() {
        List<String[][]> result = new LinkedList<>();
        for (String[][] r : permute((String[][]) singles(), 3)){
            if (!noTransitivity(r)) {
                result.add(r);
            }
        }
        return result.toArray();
    }

    /**Отфильтровать сравнения ФИО из двух частей и акронима - сравнение с третьим не транзитивно
     * */
    private static boolean noTransitivity(String[][] arr){
        boolean metAcronym = false, metTwoParts = false;
        for ( String[] ss : arr ){
            if (ss.length < 3){
                metTwoParts = true;
            }
            for ( String s : ss ){
                if (s.charAt(s.length()-1) == '~'){
                    metAcronym = true;
                }
            }
        }
        int _01 = compareNames(arr[0], arr[1]);
        int _12 = compareNames(arr[1], arr[2]);

        return metAcronym & metTwoParts || ( _01 == -_12 & _01 != 0 );
    }
}
