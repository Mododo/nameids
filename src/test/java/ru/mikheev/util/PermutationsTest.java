package ru.mikheev.util;

import org.testng.Assert;
import org.testng.annotations.Test;

import static ru.mikheev.util.Permutations.permute;

public class PermutationsTest {
    @Test(skipFailedInvocations = true)
    public void test3by2() {
        String[][][] expected = expected3by2();
        String[][][] actual = permute(new String[][]{{"1"},{"2"},{"3"}}, 2);
        test(expected, actual);
    }

    @Test(skipFailedInvocations = true)
    public void test3by3() {
        String[][][] expected = expected3by3();
        String[][][] actual = permute(new String[][]{{"1"}, {"2"}, {"3"}}, 3);
        test(expected, actual);
    }

    private void test(String[][][] expected, String[][][] actual) {
        Assert.assertEquals(expected.length, actual.length);
        for (int i = 0; i < expected.length; i++) {
            for (int j = 0; j < expected[i].length; j++) {
                Assert.assertEquals(actual[i][j], expected[i][j]);
            }
        }
    }


    private String[][] combine3by2(){
        return new String[][]{
                new String[]{"1", "2", null},
                new String[]{"2", "1", null},
        };
    }

    private String[][] combine3by3(){
        return new String[][]{
                new String[]{"1", "2", "3"},
                new String[]{"1", "3", "2"},

                new String[]{"2", "1", "3"},
                new String[]{"2", "3", "1"},

                new String[]{"3", "1", "2"},
                new String[]{"3", "2", "1"},
        };
    }


    private String[][][] expected3by2(){
        return new String[][][]{
                new String[][]{{"1"}, {"1"}},
                new String[][]{{"1"}, {"2"}},
                new String[][]{{"1"}, {"3"}},

                new String[][]{{"2"}, {"1"}},
                new String[][]{{"2"}, {"2"}},
                new String[][]{{"2"}, {"3"}},

                new String[][]{{"3"}, {"1"}},
                new String[][]{{"3"}, {"2"}},
                new String[][]{{"3"}, {"3"}}
        };
    }

    private String[][][] expected3by3(){
        return new String[][][]{
                new String[][]{{"1"}, {"1"}, {"1"}},
                new String[][]{{"1"}, {"1"}, {"2"}},
                new String[][]{{"1"}, {"1"}, {"3"}},
                new String[][]{{"1"}, {"2"}, {"1"}},
                new String[][]{{"1"}, {"2"}, {"2"}},
                new String[][]{{"1"}, {"2"}, {"3"}},
                new String[][]{{"1"}, {"3"}, {"1"}},
                new String[][]{{"1"}, {"3"}, {"2"}},
                new String[][]{{"1"}, {"3"}, {"3"}},

                new String[][]{{"2"}, {"1"}, {"1"}},
                new String[][]{{"2"}, {"1"}, {"2"}},
                new String[][]{{"2"}, {"1"}, {"3"}},
                new String[][]{{"2"}, {"2"}, {"1"}},
                new String[][]{{"2"}, {"2"}, {"2"}},
                new String[][]{{"2"}, {"2"}, {"3"}},
                new String[][]{{"2"}, {"3"}, {"1"}},
                new String[][]{{"2"}, {"3"}, {"2"}},
                new String[][]{{"2"}, {"3"}, {"3"}},

                new String[][]{{"3"}, {"1"}, {"1"}},
                new String[][]{{"3"}, {"1"}, {"2"}},
                new String[][]{{"3"}, {"1"}, {"3"}},
                new String[][]{{"3"}, {"2"}, {"1"}},
                new String[][]{{"3"}, {"2"}, {"2"}},
                new String[][]{{"3"}, {"2"}, {"3"}},
                new String[][]{{"3"}, {"3"}, {"1"}},
                new String[][]{{"3"}, {"3"}, {"2"}},
                new String[][]{{"3"}, {"3"}, {"3"}}
        };
    }
}
