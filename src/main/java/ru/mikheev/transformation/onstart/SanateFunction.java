package ru.mikheev.transformation.onstart;

import org.apache.spark.api.java.function.MapPartitionsFunction;
import org.apache.spark.sql.Row;
import ru.mikheev.pojo.FileData;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.StreamSupport;

/**Функция для стандартизации дат, которая должна быть первой операцией над данными.
 * По стандартным датам будут разбиты задания, чтобы с одной датой работал только один worker.
 * */
public class SanateFunction implements MapPartitionsFunction<Row,FileData> {
    // для разбора даты
    private static final Pattern NOT_A_NUMBER = Pattern.compile("[^0-9]+");
    private static final int CURRENT_YEAR_WITHOUT_CENTURY =
            Calendar.getInstance().get(Calendar.YEAR) - 2000;
    // для разбора имени
    private static final Pattern FIO_REDUNDANT = Pattern.compile("[^A-Za-z .]+");
    private static final Pattern DOTS = Pattern.compile("[.]+");
    private static final Map<String,String> REPLACE_RULES;
    static {
        // Правила замены подстрок в ФИО. Будут применены в последовательности добавления
        // Пока основные правила:
        // 1) правила следуют в порядке убывания длины ключа (исходной подстроки)
        // 2) каждая замена не увеличивает число букв
        // 3) только латиница в верхнем регистре
        // 4) каждая подстрока может иметь только один вариант замены (поэтому карта)
        // 5) повторения убираются до и после применения правил, их не нужно добавлять в карту
        Map<String,String> map = new LinkedHashMap<>();
        map.put("SHCH", "SCH");
        map.put("EYE", "EE");
        map.put("TCH", "CH");
        map.put("JA", "IA");
        map.put("YA", "IA");
        map.put("JE", "E");
        map.put("OF", "OV");
        map.put("KS", "X");
        map.put("KH", "H");
        map.put("Y", "I");
        // привести типичные женские окончания фамилий к мужскому роду
        // должны быть согласованы с заменами внутри ФИО
        // убрать "а" после "EV", "OV", "IN" ("-ина", "-ына"), "I" ("-ая") на конце
        map.put("EVA ", "EV ");
        map.put("OVA ", "OV ");
        map.put("INA ", "IN ");
        map.put("IA ", "I ");
        REPLACE_RULES = Collections.unmodifiableMap(map);
    }

    @Override
    public Iterator<FileData> call(Iterator<Row> input) {
        // используем поток, чтобы не создавать временных списков
        Iterable<Row> iterable = () -> input;
        // TODO более осмысленный сплитератор, чем дефолт
        return StreamSupport.stream(iterable.spliterator(), false)
                .map(SanateFunction::sanate)
                .iterator();
    }

    /**Исправляем даты
     * */
    private static FileData sanate(Row input) {
        FileData data = new FileData();
        String fio = (String) input.get(0);
        String bdate = (String) input.get(1);
        data.setFio(fio);
        data.setMutatedFio(sanateFio(fio));
        data.setBdate(sanateBdate(bdate));
        // нам нужна сортировка по дате и убыванию длины фио, spark делает ASC
        // TODO может ли не хватить 1000 символов для ФИО?
        data.setMutatedFioLength(String.valueOf(data.getMutatedFio().length()));
        return data;
    }

    static String sanateFio( String fio ){
        // Убрать символы кроме букв, точек и пробелов
        fio = FIO_REDUNDANT.matcher(fio).replaceAll(" ");
        // Привести к верхнему регистру
        fio = fio.toUpperCase();
        // Убрать все повторяющиеся подряд символы
        StringBuilder sb = sequenceOfUniques(fio);
        // пробел на конце для распознавания окончания фамилий
        sb.append(' ');
        // Применить правила замены
        sb = applyReplacements(sb);
        // Убрать все повторяющиеся подряд символы - повторно
        sb = sequenceOfUniques(sb);
        // Заменить точки на тильды и убрать пробел на конце
        return DOTS.matcher(sb).replaceAll("~").trim();
    }

    /**Применить правила замены
     * */
    static StringBuilder applyReplacements(StringBuilder sb) {
        for (Map.Entry<String, String> e : REPLACE_RULES.entrySet()){
            replaceAllOccurancies(sb, e.getKey(), e.getValue());
        }
        return sb;
    }

    /**Заменить все вхождения подстроки
     * TODO убывание подстрок по длине в списке правил позволяет переписать на inplace работу с char[]
     * чтобы не вызывалось каждый раз arrayCopy
     * нужно будет быть толерантным к пропуску символов
     *  */
    private static void replaceAllOccurancies(StringBuilder sb, String toReplace, String replacement){
        for ( int i = sb.indexOf(toReplace) ; i >= 0 ; i = sb.indexOf(toReplace)){
            sb.replace(i, i + toReplace.length(), replacement);
        }
    }

    /**Убрать повторяющиеся символы
     *  */
    static StringBuilder sequenceOfUniques(CharSequence in) {
        StringBuilder out = new StringBuilder();
        for (int i = 0; i < in.length(); i++) {
            char ch = in.charAt(i);
            if (out.length() == 0 || out.charAt(out.length()-1) != ch) {
                out.append(ch);
            }
        }
        return out;
    }

    /**Привести дату к формату ГГГГ.ММ.ДД
     * TODO привести к числу ГГГГММДД
     * @throws RuntimeException при нераспознанном формате даты
     * */
    static String sanateBdate( String bdate ){
        // берем числа, сохраняем порядок
        String[] partsStr = NOT_A_NUMBER.split(bdate);
        int[] parts = new int[partsStr.length];
        for (int i = 0; i < partsStr.length; i++) {
            parts[i] = Integer.parseInt(partsStr[i]);
        }
        // проверяем на соответстве формату
        // TODO сейчас год, месяц и день обязательны, и любые разделители между ними
        int year = 0, month = 0, day = 0;
        boolean found = false;
        if ( parts.length == 3 ){
            if ( !found && parts[0] > 31 && parts[1] > 12 ){ // ГГ.ДД.ММ
                year = parts[0];
                month = parts[2];
                day = parts[1];
                found = true;
            }
            if ( !found && parts[2] > 31 && parts[1] > 12 ){ // ММ.ДД.ГГ
                year = parts[2];
                month = parts[0];
                day = parts[1];
                found = true;
            }
            if ( !found && parts[2] > 31 ){ // ДД.ММ.ГГ
                year = parts[2];
                month = parts[1];
                day = parts[0];
                found = true;
            }
            if ( !found /* && parts[0] > 31 */ ){ // ГГ.ММ.ДД как по ТЗ
                year = parts[0];
                month = parts[1];
                day = parts[2];
                found = true;
            }
        }
        if (found){ // подготвим данные к выводу
            // порядок проверок - по убыванию диапазонов.
            if ( year >= 100 & year < 1812 ){
                // забыли тысячу. Свидетели Наполеона не дожили до эры мобильных
                year += 1000;
            }
            if ( year > CURRENT_YEAR_WITHOUT_CENTURY && year < 100 ) {
                // год из прошлого века
                year += 1900;
            }
            if ( year < CURRENT_YEAR_WITHOUT_CENTURY ) {
                // двузначный год относим к текущему веку - молодежи больше, чем долгожителей
                year += 2000;
            }
            return String.format("%d.%02d.%02d", year, month, day);
        } else {
            throw new RuntimeException("Unrecognized input date format for " + bdate);
        }
    }


    /** Можно использовать новый экземпляр вместо лямбды (альтернатива)
     * в {@linkplain SanateFunction#call(Iterator)}:
     * Iterable<> iterable = () -> input;
     * */
    @Deprecated
    private static class InputDataIterable implements Iterable<Row> {
        final Iterator<Row> iterator;

        InputDataIterable(Iterator<Row> iterator) {
            this.iterator = iterator;
        }

        @Override
        public Iterator<Row> iterator() {
            return iterator;
        }
    }
}