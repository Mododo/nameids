package ru.mikheev.transformation.id;

import ru.mikheev.pojo.FileData;
import ru.mikheev.util.Permutations;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Vector;
import java.util.concurrent.ConcurrentSkipListMap;

/** Сравнение строк с сокращениями
 * */
public class FioDictionary {
    private final ConcurrentSkipListMap<String[], String> map
            = new ConcurrentSkipListMap<>( new FioComparator() );
    private Vector<FileData> list = new Vector<>();
    /**
     * Добавить в словарь
     *
     * @return новое значение
     */
    public void put(FileData data) {
        String oldValue;
        String[][] newKeys = sortNames(data.getBdate(), data.getMutatedFio());
        for (String[] names : newKeys){
            oldValue = map.get(names);
            if (oldValue!=null){
                data.setId(oldValue);
                list.add(data);
                return;
            }
        }
        for (String[] names : newKeys){
            map.put(names, data.getId());
        }
        list.add(data);
    }

    public FileData[] getData(){
        return list.toArray(new FileData[list.size()]); // TODO stream.toArray?
    }

    /**Именованный класс, чтобы не смотреть на автосгенеренные имена в стектрейсах
     * */
    private static class FioComparator implements Comparator<String[]> {
        @Override
        public int compare(String[] o1, String[] o2) {
            return compareNames(o1, o2);
        }
    }

    /**Сравнивает ФИО, допуская отсутствие или сокращение одной из частей
     *
     * @param s1 массив из частей ФИО, если есть сокращение - оно на конце; пропуски не добавляются в массив
     * @param s2 массив из частей ФИО, если есть сокращение - оно на конце; пропуски не добавляются в массив
     * @throws NullPointerException на пустой строке
     * @throws RuntimeException на некоторых ошибках алгоритма
     * */
    static int compareNames(String[] s1, String[] s2) {
        if ( s1 == null || s1.length == 0 || s2 == null || s2.length == 0 ){
            throw new NullPointerException("Empty FIO encountered - should be filtered out");
        }

        // сравниваем части
        int result = Integer.MAX_VALUE;
        for( int i1 = 0, i2 = 0; i1 < s1.length && i2 < s2.length ; ){
            if ( isAcronym(s1[i1]) || isAcronym(s2[i2]) ){
                // акронимы на последних элементах массивов. можем сюда не зайти, если массивы разной длины
                result = compareNameParts( s1[i1], s2[i2] );
                break;
            } else {
                result = s1[i1].compareTo(s2[i2]);
                if ( result != 0 ){
                    break;
                } else {
                    i1++;
                    i2++;
                }
            }
        }
        if ( result == Integer.MAX_VALUE ){
            throw new RuntimeException("Loop exit without result specified");
        } else {
            return Integer.signum(result);
        }
    }

    /**Признак акронима - тильда на конце
     * */
    private static boolean isAcronym( String name ) {
        return name!=null && name.charAt(name.length() - 1) == '~';
    }

    /**
     * Посимвольное сравнение двух имен, допускаетя признак сокращения '.' на конце
     */
    static int compareNameParts(String s1, String s2) {
        char[] c1 = s1.toCharArray();
        char[] c2 = s2.toCharArray();
        // сравниваем строки до конца или первого расхождения
        for ( int i1 = 0, i2 = 0; i1 < c1.length && i2 < c2.length ; ) {
            if (c1[i1] == c2[i2]) {
                i1++; i2++; continue;
            }
            if (c1[i1] == '~' || c2[i2] == '~') {
                return 0;
            }
            if (c1[i1] > c2[i2]){
                return 1;
            } else {
                return -1;
            }
        }
        // одна строка включает другую без сокращений, осталось сверить их длины
        return Integer.compare(c1.length, c2.length);
    }

    /**
     * Упорядочить входные строки по алфавиту, как того требует алгоритм
     * по афавиту, сокращение на конце
     *
     * @implNote умеет работать с ФИО до 5 частей
     */
    static String[][] sortNames(String bdate, String fio) {
        String[] arr = fio.split(" ");

        boolean hasAcronym = false;
        // ищем сокращение и ставим его на конец
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].charAt(arr[i].length() - 1) == '~') {
                String acronym = arr[i];
                arr[i] = arr[arr.length - 1];
                arr[arr.length - 1] = acronym;
                hasAcronym = true;
            }
        }
        return Permutations.combine(bdate, arr, hasAcronym);
    }

    public static void main(String[] args) {
        for (String[] r : sortNames("1971.11.20","Алексей Б~ Звягинцев")) {
            System.out.println(Arrays.toString(r));
        }
    }
}

