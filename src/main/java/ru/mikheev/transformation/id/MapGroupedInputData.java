package ru.mikheev.transformation.id;

import org.apache.spark.api.java.function.MapGroupsWithStateFunction;
import org.apache.spark.sql.streaming.GroupState;
import ru.mikheev.pojo.FileData;

import java.util.Iterator;

/**Произвести определение id в потоке.
 * Функция вызывается один раз на пачку, а не на группу.
 * FIXME при низкой гранулярности - это провал подхода. Можно "лечить" большими тайм-аутами (совсем без них не комильфо)
 * Перед вызовом происходит shuffle. API не гарантирует сохранения порядка для любых Dataset.
 * TODO Операции над состоянием не потокобезопасны, нужно следить самому.
 */
public class MapGroupedInputData implements
        MapGroupsWithStateFunction<String, FileData, FioDictionary, FileData[]> {
    // удаляет состояние, если она не получает данные в течение часа
    private static final String TIMEOUT = "1 hour"; // TODO получать настройку из параметров приложения

    @Override
    public FileData[] call(String bdate, Iterator<FileData> iterator,
                         GroupState<FioDictionary> state) throws Exception {
        prepare(state);
        FioDictionary dictionary = state.get();
        while (iterator.hasNext()) {
            FileData data = iterator.next();
            data.setId(data.getBdate()+data.getMutatedFio());
            dictionary.put(data);
        }
        return dictionary.getData();
    }

    /**Ритуал по проверке состяния группы
     * Процедура оставлена по шаблону из javadoc {@link GroupState}
     */
    private static void prepare(GroupState<FioDictionary> state) {
        if (state.hasTimedOut()) { // триггер на тайм-ауте вызывает функцию без значений
            state.remove();
        } else if (state.exists()) {
            FioDictionary existingState = state.get();
            boolean shouldRemove = false; // сами не удаляем/обновляем состояние, но это возможно
            if (shouldRemove) {
                state.remove();
            } else { // после тайм-аута
                FioDictionary newState = new FioDictionary();
                state.update(newState);
                state.setTimeoutDuration(TIMEOUT); // обязательно сбросить хронометр
            }
        } else { // инициализация на старте
            FioDictionary initialState = new FioDictionary();
            state.update(initialState);
            state.setTimeoutDuration(TIMEOUT);
        }
    }
}