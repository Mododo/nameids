package ru.mikheev.Utils;

import org.apache.spark.sql.Dataset;

/**Утилитка для дебага
 * В режиме testMode выводит данные датасета в разбивке по секциям
 * */
public class DatasetLogger {
    private final boolean isTestMode;

    /**@param isTestMode true выводит в лог данные датасета
    * */
    public DatasetLogger(boolean isTestMode) {
        this.isTestMode = isTestMode;
    }

    public void log(Dataset<?> ds, String after){
        ds.explain();
        System.out.println("ds is streaming after " + after + ":" + ds.isStreaming());
        System.out.println("ds is local after " + after + ":" + ds.isLocal());
        if(isTestMode) {
            System.out.println(ds.javaRDD().glom().collect());
        }
        System.out.println("------------------------------");
    }
}
