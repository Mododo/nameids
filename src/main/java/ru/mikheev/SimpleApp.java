package ru.mikheev;

import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.sql.*;
import org.apache.spark.sql.streaming.GroupStateTimeout;
import ru.mikheev.Utils.DatasetLogger;
import ru.mikheev.pojo.FileData;
import ru.mikheev.transformation.id.FioDictionary;
import ru.mikheev.transformation.id.MapGroupedInputData;
import ru.mikheev.transformation.onstart.SanateFunction;

import java.util.Arrays;
import java.util.Iterator;

import static org.apache.spark.sql.functions.asc;
import static org.apache.spark.sql.functions.desc;
import static ru.mikheev.pojo.FileData.*;

public class SimpleApp {
    private static Encoder<FileData> encoder = Encoders.bean(FileData.class);
    public static void main(String[] args) {
        final String inputFile = args[0];
        final String outputDir = args[1];
        final Boolean testMode = Boolean.parseBoolean(args[2]);
        DatasetLogger logger = new DatasetLogger(testMode);
        SparkSession spark = SparkSession.builder().appName("NumIds").getOrCreate();

        // читаем файл в типизированный датасет
        // TODO переделать на FileStreamSource? Как потом агрегировать перед сортировкой?
        // Для FileStreamSource:
        //spark.readStream().option("sep", "|").option("header", true).option("inferSchema", true)
        //        .schema(FileData.STRUCT).format("csv").load(inputFile);
        Dataset<Row> in = spark.read().option("sep", "|").option("header", true).csv(inputFile);
        logger.log(in, "creation");

        // исправляем даты, создается новый датасет
        // работа не зависит от состояния, индивидаульна для каждй записи, поэтому можно обрабатывать по исходным блокам
        Dataset<FileData> ds = in.mapPartitions(new SanateFunction(), encoder);
        logger.log(ds, "mapPartitions");

        ds = ds.toDF(BDATE, FIO, ID, MUTATED_FIO, MUTATED_FIO_LENGTH)
                .sort(asc(BDATE),desc(MUTATED_FIO_LENGTH)).as(encoder);
        logger.log(in, "orderBy BDATE, MUTATED_FIO_LENGTH");

        // группируем по дате, чтобы сравнивать имена в пределах каждого отдельного значения
        KeyValueGroupedDataset<String, FileData> k =
                ds.groupByKey(FileData::getBdate, Encoders.STRING());

        // TODO доделать MultyAcronymNormalyzer, этот FioDictionary заготовка
        Dataset<FileData[]> g = k.mapGroupsWithState(
                new MapGroupedInputData(),
                Encoders.javaSerialization(FioDictionary.class),
                Encoders.javaSerialization(FileData[].class),
                GroupStateTimeout.ProcessingTimeTimeout()
        );
        logger.log(ds, "mapGroupsWithState");

        ds = g.flatMap(new FlatMapFunction<FileData[], FileData>() {
            @Override
            public Iterator<FileData> call(FileData[] fileData) throws Exception {
                return Arrays.stream(fileData).iterator();
            }
        }, encoder);

        // Выводим данные в файл (если тест - то в один)
        if (testMode) {
            ds = ds.coalesce(1);
        }
        ds.orderBy(ID).select(ID, FIO, BDATE)
                .write().option("header", true).option("sep", "|").csv(outputDir);
//                .writeStream().format("csv")
//                .option("checkpointLocation", outputDir+"\\checkpoint")
//                .option("path", outputDir)
//                .start();

        spark.stop();
    }

}