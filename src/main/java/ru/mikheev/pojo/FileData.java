package ru.mikheev.pojo;

import java.io.Serializable;

/**Анемичный класс для входных данных, чтобы использовать типизированный Dataset
 * Судя по плану:
 * <ul>
 * <li>implements Serializable не нужен из-за staticinvoke SerializeFromObject</li>
 * <li>Serializable и toString() добавлены для отладки через glom()</li>
 * - интересно, насколько дороже {@code Dataset<Row>} или RDD</li>
 * <ul/>
 * */
public class FileData implements Serializable {
    /* Для FileStreamSource
        public static final StructType struct = new StructType(new StructField[]{
                StructField.apply(FIO, DataTypes.StringType, false, null),
                StructField.apply(BDATE, DataTypes.StringType, false, null)
        });
    */

    public static final String ID = "id";
    public static final String FIO = "fio";
    public static final String BDATE = "bdate";
    public static final String MUTATED_FIO = "mutatedfio";
    public static final String MUTATED_FIO_LENGTH = "mutatedfiolength";

    private String id;
    private String fio;
    private String bdate;
    private String mutatedFio;
    private String mutatedFioLength;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public String getBdate() {
        return bdate;
    }

    public void setBdate(String bdate) {
        this.bdate = bdate;
    }

    public String getMutatedFio() {
        return mutatedFio;
    }

    public void setMutatedFio(String mutatedFio) {
        this.mutatedFio = mutatedFio;
    }

    public String getMutatedFioLength() {
        return mutatedFioLength;
    }

    public void setMutatedFioLength(String mutatedFioLength) {
        this.mutatedFioLength = mutatedFioLength;
    }

    public String toString() {
        return "{" + id + '|' + fio + '|' + bdate + '}';
    }
}
