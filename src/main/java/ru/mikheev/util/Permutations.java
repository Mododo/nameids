package ru.mikheev.util;

import java.util.Arrays;

public final class Permutations {
    private Permutations() {
    }

    /**@param in масиив ФИО, каждый из которых - массив строк в нужном порядке
     * @return массив комбинаций ФИО
     * */
    public static String[][][] permute(String[][] in, int arrSize) {
        String[][][] result = new String[pow(in.length, arrSize)][arrSize][];

        for (int line = 0; line < result.length; line++) {
            result[line] = new String[arrSize][];
        }

        for (int j = 0; j < arrSize; j++) {
            int period = pow(in.length, arrSize - j - 1);
            for (int line = 0; line < result.length; line++) {
                int arrPos = (line / period) % in.length;
                result[line][j] = new String[in[arrPos].length];
                for (int a = 0; a < in[arrPos].length; a++){
                    result[line][j][a] = in[arrPos][a];
                }
            }
        }
        return result;
    }

    /**Возвести целое число в степень
     * */
    private static int pow(int number, int power) {
        return (int) Math.pow(number, power);
    }

    // todo сделать тесты
    public static void main(String[] args) {
/*
        for (String[][] r : permute(new String[][]{new String[]{"1"}, new String[]{"2"}, new String[]{"3"}}, 3)) {
        for (String[] s : r) {
            System.out.print(Arrays.toString(s) + "");
        }
        System.out.println();
    }
*/
        for (String[] r : combine(new String[]{"1","2","3","4"}, true)) {
            System.out.println(Arrays.toString(r));
        }
    }

    /**Сделать перестановки и добавить к массиву элемент в начало
     * TODO слить два метода и покрыть тестами
     * */
    public static String[][] combine(String prefix, String[] in, boolean leftLastInPlace) {
        String[][] result = combine(in, leftLastInPlace);
        for (int i = 0; i < result.length; i++) {
            String[] temp = result[i];
            result[i] = new String[temp.length+1];
            result[i][0] = prefix;
            System.arraycopy(temp, 0, result[i], 1, temp.length);
        }
        return result;
    }

    /** Перестановки, которые могут оставлять на месте последний элемент
     * */
    public static String[][] combine(String[] in, boolean leftLastInPlace) {
        final int arrSize = in.length - ( leftLastInPlace ? 1 : 0 );
        String[][] result = new String[ factor( arrSize )  ][in.length];

        for (int line = 0; line < result.length; line++) {
//            result[line] = new String[arrSize];
            result[line] = Arrays.copyOf(in, in.length);
            if ( line != result.length -1 ){
                int swapFrom = in.length - 1 - ( leftLastInPlace ? 1 : 0 );
                int swapTo = in.length - 1 - generator[line] - ( leftLastInPlace ? 1 : 0 );
                String temp = in[swapFrom];
                in[swapFrom] = in[swapTo];
                in[swapTo] = temp;
            }
        }
        return result;
    }

    private static int[] generator = new int[]{ // перестановки до 5, надо подобрать производящую функцию
            1,2,1,2,1,3,1,2,1,2,1,3,1,2,1,2,1,3,1,2,1,2,1,4,
            1,2,1,2,1,3,1,2,1,2,1,3,1,2,1,2,1,3,1,2,1,2,1,4,
            1,2,1,2,1,3,1,2,1,2,1,3,1,2,1,2,1,3,1,2,1,2,1,4,
            1,2,1,2,1,3,1,2,1,2,1,3,1,2,1,2,1,3,1,2,1,2,1,4,
            1,2,1,2,1,3,1,2,1,2,1,3,1,2,1,2,1,3,1,2,1,2,1,5
    };

    private static int factor(int i){
        if (i == 0) return 1;
        return i*factor(i-1);
    }
}