:: входной файл на текущем диске
set inputFile="\tmp\sparkIn\data.csv"
:: директория для csv с ответом, на текущем диске
set outputDir="\tmp\sparkOut"
:: вывод в лог дорогого дебага, как, например, dataset.javaRDD().glom().collect
:: true - включено, любое другое значение - отключено
set testMode=true
:: перемещаю собранный jar на диск к spark_home
copy C:\git\nameIds\target\sparktask-1.0.jar %SPARK_HOME%\sparktask-1.0.jar
:: текущий диск для исполнения - D, все файлы со спарком и для спарка - там
D:
:: каждый раз убираю результат предыдущего запуска
rd /s /q \tmp\sparkOut
cd %SPARK_HOME%
bin\spark-submit2.cmd^
 --conf spark.local.dir=\tmp\spark^
 --master local[4]^
 --class "ru.mikheev.SimpleApp" ^
 sparktask-1.0.jar %inputFile% %outputDir% %testMode% ^
 > D:\tmp\log.txt